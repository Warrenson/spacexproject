-- Database creation script for the SpaceXProject

#---------------------------------------
-- Database administration
#---------------------------------------
CREATE DATABASE IF NOT EXISTS spacexproject ;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
USE spacexproject ;


# -----------------------------------------------------------------------------
#       TABLE : CONVERSATION
# -----------------------------------------------------------------------------
CREATE TABLE CONVERSATION (
   id_conversation INT AUTO_INCREMENT,
   title VARCHAR(200),
   PRIMARY KEY (id_conversation)
);

# -----------------------------------------------------------------------------
#       TABLE : MESSAGE
# -----------------------------------------------------------------------------
CREATE TABLE MESSAGE (
   id_message INT AUTO_INCREMENT,
   id_conversation INT,
   message_time DATETIME,
   message VARCHAR(5000),
   is_bot_message BOOLEAN,
   PRIMARY KEY (id_message),
   FOREIGN KEY (id_conversation) REFERENCES CONVERSATION (id_conversation)
);


# -----------------------------------------------------------------------------
#       TEST DATA TO INSERT IN THE TABLES
# -----------------------------------------------------------------------------

-- CONVERSATION DATA
INSERT INTO CONVERSATION (id_conversation, title) VALUES
(1, 'Conversation 1'),
(2, 'Conversation 2'),
(3, 'Conversation 3');

-- MESSAGE DATA
INSERT INTO MESSAGE (id_conversation, id_message, message_time, message, is_bot_message) VALUES
(1, 1, '2022-01-01 10:00:00', 'Hello from UserA', 0),
(2, 2, '2022-01-02 12:30:00', 'Hi there from UserB', 0),
(3, 3, '2022-01-03 15:45:00', 'Greetings from UserC', 0),
(1, 4, '2022-01-04 18:00:00', 'Automated response', 1);
