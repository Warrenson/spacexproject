export const LANGEN = {
  message: {
    title: "Welcome to the SpaceBOT!",
    pargraphe1:
      "Your virtual companion designed specifically to simplify your experience by answering your questions and guiding you through exciting information related to SpaceX's space activities.",
    pargraphe2:
      "You don't need any technical expertise to interact with our intelligent assistant. Just send messages via the integrated messaging system or directly select an option from the left menu, and the chatbot will provide automatic responses based on your choices.",
    pargraphe3:
      "To make this experience even richer, our chatbot leverages an external API specialized in AI-based responses, ensuring informative and friendly interactions.",
    pargraphe4:
      "Explore space effortlessly with our SpaceX chatbot, your dedicated virtual guide to the wonders of space exploration!",
  },
  input: {
    placeholder: "Ask your question...",
    lang: {
      fr: "Français",
      en: "English",
      it: "Italiano",
      es: "Español",
    },
  },
  menu: {
    title: "Don't know where to start?",
    question1: "Give me informations about the SpaceX company.",
    question2: "What does SpaceX mean by dragon?",
    question3: "Tell me about one historical event SpaceX was involved in.",
    question4: "What is the average weight of a ship?",
  },
};
