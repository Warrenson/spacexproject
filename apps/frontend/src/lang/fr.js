export const LANGFR = {
  message: {
    title: "Bienvenue sur le SpaceBOT !",
    pargraphe1:
      "Votre compagnon virtuel conçu spécialement pour simplifier votre expérience en répondant à vos questions et en vous guidant à travers les informations passionnantes liées aux activités spatiales de SpaceX.",
    pargraphe2:
      "Vous n'avez besoin d'aucune expertise technique pour interagir avec notre assistant intelligent. Il vous suffit d'envoyer des messages via la messagerie intégrée, ou bien directement sélectionner une option sur le menu de gauche, et le chatbot vous fournira des réponses automatiques en fonction de vos choix.",
    pargraphe3:
      "Pour rendre cette expérience encore plus riche, notre chatbot exploite une API externe spécialisée dans les réponses basées sur l'intelligence artificielle, assurant ainsi des interactions informatives et conviviales.",
    pargraphe4:
      "Explorez l'espace en toute simplicité avec notre chatbot SpaceX, votre guide virtuel dédié aux merveilles de l'exploration spatiale !",
  },
  input: {
    placeholder: "Posez votre question...",
    lang: {
      fr: "Français",
      en: "English",
      it: "Italiano",
      es: "Español",
    },
  },
  menu: {
    title: "Vous ne savez pas par où commencer ?",
    question1: "Donnez-moi des informations sur l'entreprise SpaceX.",
    question2: "Que veut dire SpaceX par Dragon ?",
    question3:
      "Parlez-moi d'un événement historique auquel SpaceX a participé.",
    question4: "Quel est le poids moyen d'un vaisseau spatial ?",
  },
};
