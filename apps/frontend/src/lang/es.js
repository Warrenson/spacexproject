export const LANGES = {
  message: {
    title: "¡Bienvenido al SpaceBOT!",
    pargraphe1:
      "Tu compañero virtual diseñado específicamente para simplificar tu experiencia al responder tus preguntas y guiarte a través de información emocionante relacionada con las actividades espaciales de SpaceX.",
    pargraphe2:
      "No se necesita experiencia técnica para interactuar con nuestro asistente inteligente. Simplemente envía mensajes a través del sistema de mensajería integrado o selecciona una opción del menú izquierdo, y el chatbot proporcionará respuestas automáticas según tus elecciones.",
    pargraphe3:
      "Para enriquecer aún más esta experiencia, nuestro chatbot utiliza una API externa especializada en respuestas basadas en inteligencia artificial, asegurando interacciones informativas y amigables.",
    pargraphe4:
      "Explora el espacio sin esfuerzo con nuestro chatbot de SpaceX, tu guía virtual dedicada a las maravillas de la exploración espacial.",
  },
  input: {
    placeholder: "Haga su pregunta...",
    lang: {
      fr: "Français",
      en: "English",
      it: "Italiano",
      es: "Español",
    },
  },
  menu: {
    title: "¿No sabes por dónde empezar?",
    question1: "Dame información sobre la empresa SpaceX.",
    question2: "¿Qué significa SpaceX con respecto a 'dragon'?",
    question3:
      "Háblame sobre un evento histórico en el que SpaceX estuvo involucrado.",
    question4: "¿Cuál es el peso promedio de una nave espacial?",
  },
};
