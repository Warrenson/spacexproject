import { render, screen } from '@testing-library/react';
import { describe, expect, it } from 'vitest';
import App from '../App';
import userEvent from '@testing-library/user-event';

describe('shoulbe check apps render', () => {
  it('should be check click button open chat ', async () => {
    const user = userEvent.setup();

    render(<App />)

    const buttonOpenChat = await screen.getByRole("button")


    expect(buttonOpenChat).toBeVisible();

    await user.click(screen.getByRole("button"));

    expect(buttonOpenChat).not.toBeVisible();

    expect(await screen.getByTestId("ChatBot")).toBeVisible();
  });
});
