import { beforeEach, describe, expect, test } from "vitest";
import { MessagesList } from "../MessagesList";
import { render, screen } from "@testing-library/react";

describe("<MessagesList/>", () => {

    beforeEach(() => {
        const data = [{
            "isYou": true,
            "message": "rwrwr1"
        }, {
            "isYou": false,
            "message": "rwrwr2"
        }, {
            "isYou": true,
            "message": "rwrwr3"
        }, {
            "isYou": false,
            "message": "rwrwr4"
        }]

        render(<div id="zoneMessage">
            <MessagesList messages={data} />
        </div>)
    })
    test("should be check visible message", async () => {

        expect(await screen.getByText("rwrwr1")).toBeVisible()
    })

    test("should be check visible message 2", async () => {
        expect(await screen.getByText("rwrwr2")).toBeVisible()
    })

    test("should be check visible message 3", async () => {
        expect(await screen.getByText("rwrwr3")).toBeVisible()
    })

    test("should be check visible message 4", async () => {
        expect(await screen.getByText("rwrwr4")).toBeVisible()
    })

    test("should be check message you right and left", async () => {
        const listMessage = await screen.findAllByTestId("message");

        expect(listMessage.length).toBe(4)

        expect(listMessage[0].classList[1]).toBe("messageslist__message--right")

        expect(listMessage[1].classList[1]).toBe("messageslist__message--left")
    })
})