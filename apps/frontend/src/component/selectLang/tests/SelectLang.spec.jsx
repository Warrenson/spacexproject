import { render, screen } from "@testing-library/react";
import { I18nextProvider } from "react-i18next";
import { beforeEach, describe, expect, test, vi } from "vitest";
import i18n from "../../../i18n";
import { SelectLang } from "../SelectLang";
import userEvent from "@testing-library/user-event";
import { LANGEN } from "../../../lang/en";

describe("<SelectLang/>", () => {
    const onClose = vi.fn();
    const user = userEvent.setup();

    beforeEach(() => {
        render(
            <I18nextProvider i18n={i18n}>
                <SelectLang onClose={onClose} />
            </I18nextProvider>);
    })

    test("should be check select lang call props", async () => {
        const labelfr = await screen.getByText(LANGEN.input.lang.fr);

        await user.click(labelfr);

        expect(onClose).toHaveBeenCalledTimes(1);
    })

    test("should be Check all lang is present", async () => {
        const listLang = Object.values(LANGEN.input.lang);

        expect(await screen.findAllByTestId("select-lang-items"));

        listLang.forEach(async (value) => {
            const inputLang = await screen.getByText(value);
            expect(inputLang).toBeVisible();
        })
    })
})