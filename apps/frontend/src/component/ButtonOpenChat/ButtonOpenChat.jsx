import ButtonIconSvg from '../../assets/button.svg?react';
import "./ButtonOpenChat.scss"
export function ButtonOpenChat({ onClick }) {
    return (
        <button onClick={onClick} className='buttonIcon'>
            <ButtonIconSvg />
        </button>
    )
}
