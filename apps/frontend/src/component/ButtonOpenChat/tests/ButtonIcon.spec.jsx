import { describe, expect, test, vi } from "vitest";
import { ButtonOpenChat } from "../ButtonOpenChat";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

describe("<ButtonIcon />", () => {
    test("shoulbe check button is visable", async () => {
        render(<ButtonOpenChat />);

        expect(await screen.getByRole("button")).toBeInTheDocument()
    })

    test("should check button use onClick props", async () => {
        const onClick = vi.fn();
        const user = userEvent.setup()

        render(<ButtonOpenChat onClick={onClick} />);

        await user.click(screen.getByRole("button"));

        expect(onClick).toHaveBeenCalled();

        expect(onClick).toHaveBeenCalledTimes(1);
    })
})