import { useTranslation } from 'react-i18next'
import SpaceXLogo from '../../assets/SpaceX_logo.png'
import './ZoneTextDefault.scss'
export function ZoneTextDefault() {
    const { t } = useTranslation();

    return (
        <div className="zoneText">
            <img src={SpaceXLogo} alt="logo X spaceX" />
            <h4>{t("message.title")}</h4>
            <p>{t("message.pargraphe1")}</p>
            <p>{t("message.pargraphe2")}</p>
            <p>{t("message.pargraphe3")}</p>
            <p>{t("message.pargraphe4")}</p>
        </div>
    )
}
