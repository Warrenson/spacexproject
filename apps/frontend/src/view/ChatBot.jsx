import { useState } from 'react'
import CloseButton from '../assets/CloseButton.svg?react'
import SpaceBotIcon from '../assets/SpaceBOT.svg?react'
import { ZoneTextDefault } from '../component/ZoneTextDefault/ZoneTextDefault'
import { InputChat } from '../component/inputChat/inputChat'

import './ChatBot.scss'
import { MessagesList } from '../component/messagesList/MessagesList'
import { ButtonMenu } from '../component/Buttonmenu/ButtonMenu'
import { Menu } from '../component/Menu/Menu'

export function ChatBot({ onClose }) {
    const [messages, setMessages] = useState([/*{
        "isYou": true,
        "message": "rwrwr"
    }, {
        "isYou": false,
        "message": "rwrwr"
    }, {
        "isYou": true,
        "message": "rwrwr"
    }, {
        "isYou": false,
        "message": "rwrwr"
    }*/])
    const [isOpenMenu, setIsOpenMenu] = useState(false)

    const onMenuHandle = () => {
        setIsOpenMenu(state => !state);
    }

    const sendMessage = (message) => {
        setMessages(state => [
            ...state,
            {
                isYou: true,
                message
            }
        ])
    }
    return (
        <div data-testid="ChatBot" className="ChatBot">
            <div className="ChatBot__header">
                <div className='ChatBot__header__title'>
                    <h1>
                        <SpaceBotIcon />
                    </h1>
                </div>
                {isOpenMenu ? <Menu onSend={sendMessage} onClose={onMenuHandle} /> : <ButtonMenu onClick={onMenuHandle} />}
                <div className='ChatBot__header__CloseButton'>
                    <button onClick={onClose}>
                        <CloseButton width={"25px"} height={"25px"} />
                    </button>
                </div>
            </div>
            <div className="ChatBot__ZoneMessage" id="zoneMessage">
                {messages.length === 0 ? <ZoneTextDefault /> : <MessagesList messages={messages} />}
            </div>
            <div className="ChatBot__footer">
                <InputChat onSend={sendMessage} />
            </div>
        </div>
    )
}

