<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class Test extends TestCase
{
    public function testOnePlusOne(): void
    {
        $this->assertSame(1 + 1, 2);
    }
}